# StudentReport

# Overview

We will be building an API layer to extract, message and present data from a mongoDB. The database contains data from a university. Each entry indicates the performance of a student in a course.

Before we begin building the API, let’s aim to understand the data we are dealing with. To this end, answer the following (your submission in github or similar repo should have the answers as functions called func_Q1, func_Q2 etc.). pymongo.database. Database object is a single parameter that is passed to these functions.


1. Students
    1. Get all students

        **Description**: This endpoint retrieves the list of students from the database and returns an array of students objects to the user.

        **Path**: `/students` ****

        **Method**: GET

        **Status**: 200

        **Success response**:

            [
            	{
            	  "student_id": integer,
                "student_name": string
              },
              {
            	  "student_id": integer,
                "student_name": string
              }
            ]

    2. Get list of classes for a student

        **Description**: This endpoint retrieves the list of classes that are taken by a particular student whose `student_id` is supplied as part of the API URL.

        **Path**: `/student/{student_id}/classes`

        **Method**: GET

        **Status**: 200

        **Success response**:

            {
            	"student_id": integer,
            	"student_name": string,
            	"classes": [
            		{
            			"class_id": integer
            		},
            		{
            			"class_id": integer
            		}
            	]
            }

    3. Get aggregate performance in each class for a student

        **Description**: This endpoint retrieves the list of classes that are taken by a particular student and for each class, returns the total marks obtained by the student in that class (simple sum across different exams/homework etc. for the class).

        **Path**: `/student/{student_id}/performance`

        **Method**: GET

        **Status**: 200

        **Success response**:

            {
            	"student_id": integer,
            	"student_name": string,
            	"classes": [
            		{
            			"class_id": integer,
            			"total_marks": integer
            		},
            		{
            			"class_id": integer,
            			"total_marks": integer
            		}
            	]
            }

2. Classes
    1. Get all classes

        **Description**: This endpoint retrieves the list of classes for which we have any data in the database.

        **Path**: `/classes`

        **Method**: GET

        **Status**: 200

        **Success response**:

            [
            	{
            		"class_id": integer
            	},
            	{
            		"class_id": integer
            	}
            ]

    2. Get list of students taking a course

        **Description**: This endpoint retrieves the list of students who have taken a particular class.

        **Path**: `/class/{class_id}/students`

        **Method**: GET

        **Status**: 200

        **Success response**:

            {
            	"class_id": integer,
            	"students": [
            		{
            			"student_id": integer,
            			"student_name": string
            		},
            		{
            			"student_id": integer,
            			"student_name": string
            		}
            	]
            }

    3. Get aggregate performance of each student taking a course

        **Description**: This endpoint retrieves the list of students and gives the aggregate performance of each students in that class.

        **Path**: `/class/{class_id}/performance`

        **Method**: GET

        **Status**: 200

        **Success response**:

            {
            	"class_id": integer,
            	"students": [
            		{
            			"student_id": integer,
            			"student_name": string,
            			"total_marks": integer
            		},
            		{
            			"student_id": integer,
            			"student_name": string,
            			"total_marks": integer
            		}
            	]
            }

    4. (Bonus) Get grades for a particular course

        **Description**: This endpoint retrieves the list of students who have taken a particular class and gives the total grade-sheet for the course.

        **Path**: `/class/{class_id}/final-grade-sheet`

        **Method**: GET

        **Status**: 200

        **Success response**:

            {
            	"class_id": integer,
            	"students": [
            		{
            			"student_id": integer,
            			"student_name": string,
            			"details": [
            				{
            					"type": "exam",
            					"marks": integer
            				},
            				{
            					"type": "quiz",
            					"marks": integer
            				},
            				{
            					"type": "homework1",
            					"marks": integer
            				},
            				{
            					"type": "homework2",
            					"marks": integer
            				},
            				{
            					"type": "total",
            					"marks": integer
            				}
            			],
            			"grade": string
            		},
            		{
            			"student_id": integer,
            			"student_name": string,
            			"details": [
            				{
            					"type": "exam",
            					"marks": integer
            				},
            				{
            					"type": "quiz",
            					"marks": integer
            				},
            				{
            					"type": "homework1",
            					"marks": integer
            				},
            				{
            					"type": "homework2",
            					"marks": integer
            				},
            				{
            					"type": "total",
            					"marks": integer
            				}
            			],
            			"grade": string
            		}
            	]
            }

        *Note: For grade, use the logic as follows:*

        - Top 1/12th of the class (you can take floor if it comes out be a fraction) gets grade "A"
        - Next 1/6th of the class gets grade "B"
        - Next 1/4th of the class gets grade "C"
        - Rest of the class gets grade "D"
3. Student + Course
    1. Get detailed scores for a student in a course

        **Description**: This endpoint retrieves only the specific information about the marks obtained by a student in a particular course

        **Path**: `/class/{class_id}/student/{student_id}` or `/student/{student_id}/class/{class_id}` (either endpoint should work and give the same result)

        **Method**: GET

        **Status**: 200

        **Success response**:

            {
            	"class_id": integer,
            	"student_id": integer,
            	"student_name": string,
            	"marks": [
            		{
            			"type": "exam",
            			"marks": integer
            		},
            		{
            			"type": "quiz",
            			"marks": integer
            		},
            		{
            			"type": "homework1",
            			"marks": integer
            		},
            		{
            			"type": "homework2",
            			"marks": integer
            		},
            		{
            			"type": "total",
            			"marks": integer
            		}
            	]
            }



Creating an environment from an environment.yml file

Use the terminal or an Anaconda Prompt for the following steps:

    1)Create the environment from the environment.yml file:

        conda env create -f environment.yml

    2) Activate the new environment: 
        
        conda activate myenv

    3) Verify that the new environment was installed correctly:

        conda env list

    You can also use conda info --envs.


1. Run server:
    python manage.py runserver