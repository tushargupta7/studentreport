"""assignment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^students/', views.getStudentList, name='AllStudents'),
    url(r'^student/(?P<student_id>[0-9]+)/classes', views.getEnrolledClass, name='Student'),
    url(r'^student/(?P<student_id>[0-9]+)/performance', views.getStudentPerformance, name='StudentPerformance'),
    url(r'^classes/', views.getClasses, name='AllClasses'),
    url(r'^class/(?P<class_id>[0-9]+)/students', views.getClassStudents, name='ClassStudents'),
    url(r'^class/(?P<class_id>[0-9]+)/performance', views.getClassPerformance, name='ClassPerformance'),
    url(r'^class/(?P<class_id>[0-9]+)/student/(?P<student_id>[0-9]+)', views.getStudentDetailedScore, name='ClassStudents'),
    url(r'^student/(?P<student_id>[0-9]+)/class/(?P<class_id>[0-9]+)', views.getStudentDetailedScore, name='ClassStudents'),

]
