from django.http import JsonResponse
import pymongo

"""Establish Database Collection and assign Collections"""
client = pymongo.MongoClient('mongodb+srv://prodigal_be_test_01:prodigaltech@test-01-ateon.mongodb.net/sample_training')
db = client['sample_training']
#print(db.list_collection_names())
grades_coll = db['grades']
student_coll =db['students']

""" 
Retrieves the list of students from the database and returns an array of 100 students objects to the user.
Scope of work: To add itereator for more record fetches i.e pagination of record.
Request arguments include the page numbers.
"""
def getStudentList(request):
    student_query={}
    student_list=student_coll.find(student_query).limit(100)
    if student_list==None:
        return JsonResponse(status= 404,data={'status':'false','message': "No Record Found"})
    response_list=[]
    for student in student_list:
        response_list.append({'student_id':student['_id'],'student_name':student['name']})
    return JsonResponse({'status':200,'response':response_list})


"""
Retrieves the list of classes that are taken by a particular student whose student_id is supplied as part of the API URL.
"""
def getEnrolledClass(request,student_id):
    student_query = {"student_id":int(student_id)}
    student= student_coll.find_one({"_id":int(student_id)})
    if student==None :
        return JsonResponse(status= 404,data={'status':'false','message': "No Record Found"})
    grades_list=grades_coll.find(student_query)
    if grades_list==None:
        return JsonResponse(status= 404,data={'status':'false','message': "No Grades Found"})
    class_list=[]
    for student_grade in grades_list:
        class_list.append({'class_id':student_grade['class_id']})
    return JsonResponse({'status': 200, 'response':{'student_id':student_id,'student_name':student['name'],'classes':class_list}})


"""
Retrieves the list of classes that are taken by a particular student and for each class, 
returns the total marks obtained by the student in that class 
(simple sum across different exams/homework etc. for the class).
"""
def getStudentPerformance(request,student_id):
    student_query = {"student_id": int(student_id)}
    student = student_coll.find_one({"_id": int(student_id)})
    if student==None:
        return JsonResponse(status= 404,data={'status':'false','message': "No Record Found"})
    student_details = grades_coll.find(student_query)
    performance_list=[]
    for details in student_details:
        performance = 0
        for score in details['scores']:
            performance+=score['score']
        performance_list.append({'class_id':details['class_id'],'total_marks':int(performance)})
    return JsonResponse(
        {'status': 200, 'response': {'student_id': student_id, 'student_name': student['name'], 'classes': performance_list}})

"""
Retrieves the list of classes for which we have any data in the database
"""
def getClasses(request):
    class_list=grades_coll.distinct("class_id")
    if class_list==None:
        return JsonResponse(status= 404,data={'status':'false','message': "No Class Found"})
    response_list=[]
    for classes in class_list:
        response_list.append({'class_id':classes})
    return JsonResponse({'status': 200, 'response': response_list})



"""
Retrieves the list of students who have taken a particular class.
"""
def getClassStudents(request,class_id):
    student_query ={"class_id":int(class_id)}
    student_list=grades_coll.find(student_query).limit(20)
    if student_list==None:
        return JsonResponse(status= 404,data={'status':'false','message': "No Record Found"})
    response_list=[]
    for student in student_list:
        student_name=student_coll.find_one({"_id":int(student['student_id'])})['name']
        response_list.append({'student_id': student['student_id'], 'student_name': student_name})
    return JsonResponse({'status':200,'response':{'students':response_list}})


"""
Retrieves the list of students and gives the aggregate performance of each students in that class.
"""
def getClassPerformance(request,class_id):
    student_query ={"class_id":int(class_id)}
    student_list=grades_coll.find(student_query).limit(20)
    if student_list==None :
        return JsonResponse(status= 404,data={'status':'false','message': "No Record Found"})
    response_list=[]
    for student in student_list:
        student_name=student_coll.find_one({"_id":int(student['student_id'])})['name']
        performance=0
        for score in student['scores']:
            performance+=score['score']
        response_list.append({'student_id': student['student_id'], 'student_name': student_name, 'total_marks':int(performance)})
    return JsonResponse({'status':200,'response':{'class_id':class_id,'students':response_list}})


"""
Retrieves only the specific information about the marks obtained by a student in a particular course
"""
def getStudentDetailedScore(request,class_id,student_id):
    student_query ={"class_id":int(class_id)}
    student=grades_coll.find_one(student_query)
    if student==None:
        return JsonResponse(status= 404,data={'status':'false','message': "No Record Found"})
    student_name = student_coll.find_one({"_id": int(student_id)})['name']
    marks_list=[]
    for score in student['scores']:
        marks_list.append({'type':score['type'],'marks':score['score']})
    return JsonResponse({'status':200,'response':{'class_id':class_id,'student_id':student_id,'student_name':student_name,'marks':marks_list}})



